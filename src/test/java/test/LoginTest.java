package test;

import base.Testengine;
import com.nca.pages.LoginPage;
import io.qameta.allure.Description;
import org.testng.ITestListener;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class LoginTest extends Testengine implements ITestListener {
    public static LoginPage loginPage;

    @BeforeClass
    @Override
    public void setUpPage() {
        loginPage = new LoginPage(driver);
    }

    @Test(priority = 1)
    @Description("Validate button click")
    public void testLogin() throws InterruptedException {

        log.info("Running Step: verify_all_main_page_elements_are_present()");

        loginPage.clickLoginButton();


    }
}
