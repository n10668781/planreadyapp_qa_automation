package com.nca.pages;

import io.appium.java_client.AppiumDriver;
import io.qameta.allure.Step;
import org.openqa.selenium.By;

public class LoginPage extends BasePage {
    private static final By loginButton = By.xpath("//android.widget.TextView[@content-desc=\"Log in.\"]");

    public LoginPage(AppiumDriver driver) {
        super(driver);
    }

    @Step("Click on the Log in Button")
    public void clickLoginButton() {
        if (waitForElementPresent(loginButton)) {
            clickElement(loginButton);
            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
